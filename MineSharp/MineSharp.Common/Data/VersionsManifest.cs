using System.Collections.Generic;
using System.Linq;
using MineSharp.Common.Data.Internal;

namespace MineSharp.Common.Data
{
    public class VersionsManifest
    {
        /// <summary>
        /// Latest Minecraft Information Object
        /// </summary>
        public LatestInformation Latest;
        
        /// <summary>
        /// Versions Information Object List
        /// </summary>
        public List<MinecraftVersion> Versions = new List<MinecraftVersion>();

        /// <summary>
        /// Gets Minecraft Manifest Data by Version
        /// Example: version: 1.10.2
        /// </summary>
        /// <param name="version">Minecraft Version</param>
        /// <returns>Object if exists, null otherwise</returns>
        public MinecraftVersion GetVersionData(string version)
        {
            return Versions.FirstOrDefault(q => q.Id.Equals(version));
        }
    }
}