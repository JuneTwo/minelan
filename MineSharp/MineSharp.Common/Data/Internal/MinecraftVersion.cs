using System;

namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Contains infromation about Minecraft Version
    /// </summary>
    public class MinecraftVersion
    {
        /// <summary>
        /// Version ID
        /// </summary>
        public string Id;
        
        /// <summary>
        /// Version Type
        /// eg. snapshot, release, old_alpha, old_beta
        /// </summary>
        public string Type;
        
        /// <summary>
        /// Version manifest URI
        /// </summary>
        public string Url;
        
        public string Time;
        
        /// <summary>
        /// Version Release Date
        /// </summary>
        public string ReleaseTime;

        /// <summary>
        /// Returns Time string as Parsed DateTime
        /// </summary>
        /// <returns></returns>
        public DateTime GetTime()
        {
            return DateTime.Parse(Time);
        }

        /// <summary>
        /// Returns ReleaseTime string as Parsed DateTime
        /// </summary>
        /// <returns></returns>
        public DateTime GetReleaseTime()
        {
            return DateTime.Parse(ReleaseTime);
        }

    }
}