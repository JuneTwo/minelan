namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Contains information about library artifact
    /// </summary>
    public class LibraryArtifactInformation
    {
        /// <summary>
        /// Artifact path
        /// </summary>
        public string Path;
        
        /// <summary>
        /// Artifact checksum
        /// </summary>
        public string Sha1;
        
        /// <summary>
        /// Artifact size
        /// </summary>
        public long Size;
        
        /// <summary>
        /// Artifact URI
        /// </summary>
        public string Url;
    }
}