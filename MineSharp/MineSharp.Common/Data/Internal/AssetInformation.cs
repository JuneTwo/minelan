namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Represents ingame Asset
    /// </summary>
    public class AssetInformation
    {
        /// <summary>
        /// Json Title (from JProperty)
        /// </summary>
        public string Title;
        
        /// <summary>
        /// Asset hash
        /// </summary>
        public string Hash;
        
        /// <summary>
        /// Asset Size
        /// </summary>
        public long Size;

        /// <summary>
        /// Gets Asset Folder (eg. /bd/[asset_hash])
        /// </summary>
        /// <returns></returns>
        public string GetFolder()
        {
            //Asset folder should be equal to first two characters of Hash
            var str = "";
            str += Hash[0];
            str += Hash[1];
            return str;
        }
    }
}