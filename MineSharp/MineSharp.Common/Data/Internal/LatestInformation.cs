namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Contains information about latest release and snapshot
    /// </summary>
    public class LatestInformation
    {
        /// <summary>
        /// Latest game release version
        /// </summary>
        public string Release;
        
        /// <summary>
        /// Latest snapshot version
        /// </summary>
        public string Snapshot;
    }
}